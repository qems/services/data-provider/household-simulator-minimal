import collections

import mosaik_api_v3 as mosaik_api

import arrow
import datetime
import random


from . import models

import pandas as pd

META = {
    'type': 'time-based',
    'models': {
        'Household': {
            'public': True,
            'any_inputs': False,
            'params': ['inputThermalMin', 'inputThermalMax', 'inputElectricalMin', 'inputElectricalMax'],
            'attrs': ['inputThermalMin', 'inputThermalMax', 'inputElectricalMin', 'inputElectricalMax'],
        },
        'Now': {
            'public': False,
            'any_inputs': False,
            'params': [],
            'attrs': ['id', 'value']
        },
        'Prediction': {
            'public': False,
            'any_inputs': False,
            'params': [],
            'attrs': ['id', 'forecast']
        },
        'Historic': {
            'public': False,
            'any_inputs': False,
            'params': [],
            'attrs': ['id', 'history']
        }
    },
}

class HouseholdSimulator(mosaik_api.Simulator):

    def __init__(self):
        super().__init__(META)
        self.data = collections.defaultdict(lambda:
                                            collections.defaultdict(list))
        self.step_size = None

        self.influx_connection = None
        self.influx_bucket = None
        self.time_resolution = 1.0
        self.sim_start = None
        self.file_sim_start = None
        self.time_delta = None

        self.household_models = {}

    def init(self, sid, time_resolution=1.0, sim_start=0, file_sim_start=0, date_format=None, seed=42, jitter=100.0, filename='household/demand.csv', forecast_horizon='24h'):
        random.seed(a=seed)
        self.sim_start = self.get_time(sim_start, date_format)
        self.file_sim_start = self.get_time(file_sim_start, date_format)
        self.time_delta = self.sim_start - self.file_sim_start
        self.time_resolution = time_resolution

        try:
            self.historic_demand = pd.read_csv(filename,
                                        names=['time', 'electricity', 'dhw', 'heating'], index_col='time',
                                        sep=",", usecols=["time", "electricity", "dhw", "heating"], header=0, parse_dates=True)
        except FileNotFoundError:
            import pathlib
            dir_name = pathlib.Path(__file__).parent            
            if (filename.split("/")[0] in str(dir_name.absolute())):
                dir_name = dir_name.parent
            filename = dir_name / filename
                
            self.historic_demand = pd.read_csv(filename,
                                        names=['time', 'electricity', 'dhw', 'heating'], index_col='time',
                                        sep=",", usecols=["time", "electricity", "dhw", "heating"], header=0, parse_dates=True)
        self.forecast_horizon = forecast_horizon

        self.historic_demand["heating"] /= self.historic_demand["heating"].max()  # normalize to values between 0 and 1
        self.historic_demand["electricity"] /= self.historic_demand["electricity"].max()  # normalize to values between 0 and 1

        self.seed = seed
        self.jitter = jitter

        return self.meta

    def create(self, num, model, **params):
        if model == "Household":
            return_value = []
            for i in range(num):
                instance = models.Household(**params)
                return_value.append(instance.as_mosaik_meta_dict())
                self.household_models[instance.eid] = instance

            return return_value
        return None
    
    def step(self, time, inputs, max_advance):
        now = self.file_sim_start.shift(seconds=time * self.time_resolution).datetime
        now_forecast = self.file_sim_start.shift(seconds=time * self.time_resolution).datetime - datetime.timedelta(hours=1)
        
        now_forecast_pd = pd.Timestamp(now_forecast)
        now_forecast_pd_hour = now_forecast_pd.floor(freq='H')

        now_values = self.historic_demand.iloc[self.historic_demand.index.get_indexer([now_forecast_pd], method="pad", limit=1)]
        forecast_values = self.historic_demand[now_forecast_pd_hour:now_forecast_pd_hour + pd.to_timedelta(self.forecast_horizon)]

        for k in self.household_models:
            model = self.household_models[k]

            now_heating = max(0.0, (now_values["heating"].values[0] * model.inputThermalMax) + (self.jitter * random.uniform(-1.0, 1.0)))
            now_electricity = max(0.0, (now_values["electricity"].values[0] * model.inputElectricalMax) + (self.jitter * random.uniform(-1.0, 1.0)))

            forecast_heating = (forecast_values["heating"] * model.inputThermalMax).to_dict()
            forecast_heating =  {int(((k + self.time_delta).timestamp()) * 1000):forecast_heating[k] for k in forecast_heating }

            forecast_electricity = (forecast_values["electricity"] * model.inputElectricalMax).to_dict()
            forecast_electricity =  {int(((k + self.time_delta).timestamp()) * 1000):forecast_electricity[k] for k in forecast_electricity }

            model.inputThermalHeatingW[0].value = now_heating
            model.inputThermalHeatingW[1].forecast = forecast_heating
            model.inputThermalHeatingW[2].history[now.timestamp() * 1000] = now_heating  # TODO: Limit to n hours/days?
            model.inputThermalDhwW[0].value = 0.0
            model.inputThermalDhwW[1].forecast = {}
            model.inputElectricalPowerW[0].value = now_electricity
            model.inputElectricalPowerW[1].forecast = forecast_electricity
            model.inputElectricalPowerW[2].history[now.timestamp() * 1000] = now_electricity  # TODO: Limit to n hours/days?

        one_minute_in_timesteps = self.time_resolution * 60
        return time + int(one_minute_in_timesteps)
    
    def get_data(self, outputs):
        data = {}
        for k in self.household_models:
            model = self.household_models[k]
            data.update(model.get_data())  # just output everything, what do I care?

        return data

    def get_time(self, date, date_format):
        if date_format is not None:
            return arrow.get(date, self.date_format)
        else:
            return arrow.get(date)
